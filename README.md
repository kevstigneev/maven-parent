Maven parent POMs
=================

Available parent POMs and their `artifactId`:

- `parent-common` - basic parent POM, inherited by all other POMs.
- `parent-vm` - parent POM for projects building VM images by Hashicorp Packer.

Add into the root POM an application project (take `NAME` from the list above):

    <parent>
        <groupId>org.bitbucket.kevstigneev.maven-parent</groupId>
        <artifactId>NAME</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </parent>

CI friendly Maven versions
--------------------------

The included parent POMs support
[CI friendly versions](https://maven.apache.org/maven-ci-friendly.html) in
application projects. To make use of them add to the root application POM:

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <modelVersion>4.0.0</modelVersion>

        <!-- 1. Add a parent POM from this repository as the <parent>. -->
        <parent>
            <groupId>org.bitbucket.kevstigneev.maven-parent</groupId>
            <artifactId>parent-common</artifactId><!-- or any other parent POM from this repository -->
            <version>0.1.0-SNAPSHOT</version>
        </parent>

        <!-- 4. Define the project <version> using ${revision}, ${sha1} and ${changelist} properties. -->
        <version>${revision}${sha1}${changelist}</version>
        <artifactId>...</artifactId>
        <groupId>...</groupId>

        <!-- 3. Set ${revision}, ${sha1} and ${changelist} properties. They define the project version. -->
        <properties>
            <revision>0.1.0</revision>
            <sha1/>
            <changelist>-SNAPSHOT</changelist>
        </properties>

        <build>
            <plugins>

                <!-- 2. Include flatten-maven-plugin to the build configuration. -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>flatten-maven-plugin</artifactId>
                </plugin>
                ...
            </plugins>
        </build>
        ...
    </project>

In sub-modules refer to the application parent POM as following:

    <parent>
        <version>${revision}${sha1}${changelist}</version>
        <artifactId>...</artifactId>
        <groupId>...</groupId>
    </parent>

Then don't use Maven "release" or "versions" plugins to manage the project
versions. Just update `<properties>` in the root application POM or alter them
at build time. Like this to build a short version:

    mvn -Drevision= clean install
